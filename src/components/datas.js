export const emprunt = [
	{
		id: 1,
        lecteur: 'Julie',
		dateEmprunt: '05/02/2021',
		dateRetour: null,
        initialReception: 'JL'

	},
	{
		id: 2,
        lecteur: 'Max',
		dateEmprunt: '01/03/2021',
		dateRetour: null,
        initialReception: 'ML'
	},
	{
        id: 3,
        lecteur: 'Anthoine',
		dateEmprunt: '12/12/2020',
		dateRetour: '22/02/2021',
        initialReception: 'AM'
	},
	{
        id: 4,
        lecteur: 'Sophie',
		dateEmprunt: '09/11/2020',
		dateRetour: '02/02/2021',
        initialReception: 'SC'
	},
	{
        id: 5,
        lecteur: 'Cristophe',
		dateEmprunt: '18/02/2021',
		dateRetour: null,
        initialReception: 'CL'
	},
	{
        id: 6,
        lecteur: 'Anthoine',
		dateEmprunt: '30/01/2021',
		dateRetour: null,
        initialReception: 'AM'
	},
	{
		id: 7,
        lecteur: 'Nathalie',
		dateEmprunt: '29/02/2021',
		dateRetour: '20/03/2021',
        initialReception: 'NB'
	},
	{
		id: 8,
        lecteur: 'Marc',
		dateEmprunt: '13/10/2020',
		dateRetour: '24/01/2021',
        initialReception: 'MA'
	},
	{
		id: 9,
        lecteur: 'Daniel',
		dateEmprunt: '07/03/2021',
		dateRetour: null,
        initialReception: 'DO'
	}
]