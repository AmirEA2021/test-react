import '../style/banner.css'
import {Link, NavLink} from 'react-router-dom';


function Banner() {
    return (
    <div>
    <h1 className="banner">Gestion Bibliothèque</h1>
    <NavLink to="/">Home</NavLink>
    </div>)
}

export default Banner