import { useState, useEffect } from 'react';
import Axios from 'axios';
import Banner from './Banner';
import 'materialize-css'; // It installs the JS asset only
import 'materialize-css/dist/css/materialize.min.css';




function AjoutAbonnes() { 

    const [nom, setNom] = useState('')
    const [prenom, setPrenom] = useState('')
    const [courriel, setCourriel] = useState('')

    const [abonnesList, setAbonnesList] = useState([])
    

    const submitAbonnes=()=>{
        Axios.post('http://localhost:4000/api/insert', {nom : nom, prenom : prenom, courriel : courriel}).then(()=>{
            setAbonnesList([...abonnesList,{nom : nom, prenom : prenom, courriel : courriel}])
        });
    };

return(


    <div>
        <Banner/>
        <h1>Ajouter abonnes</h1>
        <div>
                <div>
                    <label>Nom  </label>
                    <input type="text" name="nom" onChange={(e)=>{
                        setNom(e.target.value)
                    }}></input>
                </div>
                <div>
                    <label>Prenom </label>
                    <input type="text" name="prenom" onChange={(e)=>{
                        setPrenom(e.target.value)
                    }}></input>
                </div>
                <div>
                    <label>Courriel </label>
                    <input type="text" name="courriel" onChange={(e)=>{
                        setCourriel(e.target.value)
                    }}></input>
                </div>
                <div>
                    <button onClick={submitAbonnes} className="waves-effect waves-light btn"> Submit</button>
                </div>
            </div>
    </div>

);

}

export default AjoutAbonnes