import '../App.css';
import Banner from './Banner';
import Emprunt from './Emprunt';
import Abonnes from './Abonnes';
import NotFound from './NotFound';
import Home from './Home';
import AjoutAbonnes from './AjoutAbonnes';
import Produit from './Produit';
import EditAbonnes from './EditAbonnes';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {Link, NavLink} from 'react-router-dom';
import Statistique from './Statistique';





function App() {

  return (
    
    <Router>
    <div>
    
     {/* ************************* Routage entre les differentes pages react-router-dom ************************* */} 
      <Switch>
      <Route  path="/" exact component={Home}/>
      <Route  path="/emprunt" component={Emprunt} />
      <Route  path="/produit" component={Produit} />
      <Route  path="/statistique" exact component={Statistique}/>
      <Route  path="/ajoutAbonnes" component={AjoutAbonnes} />
      <Route  path="/EditAbonnes/:id" component={EditAbonnes} />
      
      <Route component={NotFound}/>
      </Switch>

      
    </div>
    </Router>
  );
}

export default App;


