import { useState, useEffect } from 'react';
import Axios from 'axios';
import '../style/abonnes.css';
import Banner from './Banner';


function Emprunt() {

   
    const [id_Abonnes, setIdAbonnes] = useState('')
    const [id_Produit, setId_Produit] = useState('')
    const [date_Emprunt, setDate_Emprunt] = useState('')
    const [initial_Emprunt, setInitial_Emprunt] = useState('')

   const [abonnesList, setAbonnesList] = useState([])
   const [produitList, setProduitList] = useState([])


   useEffect(()=>{

      Axios.get('http://localhost:4000/api/getp').then((response)=>{
      /* console.log(response.data); */
      setProduitList(response.data)
      })
   
    },[])



   useEffect(()=>{

      Axios.get('http://localhost:4000/api/geta').then((response)=>{
      /* console.log(response.data); */
      setAbonnesList(response.data)
      })
   
    },[])


   const [EmpruntList, setEmpruntList] = useState([])


   useEffect(()=>{

      Axios.get('http://localhost:4000/api/get').then((response)=>{
      /* console.log(response.data); */
      setEmpruntList(response.data)
      })
   
    },[]);


    const deleteEmprunt =(id)=>{
      Axios.delete(`http://localhost:4000/api/deleteem/${id}`);
  }


    const creerEmprunt=()=>{
      Axios.post('http://localhost:4000/api/inserte', {id_Abonnes : id_Abonnes, id_Produit : id_Produit, date_Emprunt : date_Emprunt, initial_Emprunt : initial_Emprunt}).then(()=>{
          setEmpruntList([...EmpruntList,{id_Abonnes : id_Abonnes, id_Produit : id_Produit, date_Emprunt : date_Emprunt, initial_Emprunt : initial_Emprunt}])
      });
  };





   return (
      <div className="Emprunt">
       <Banner/>
            <div>
               <div>
               <h2>Liste EMPRUNT</h2>
            <table>
                  <thead>
                     <tr>
                        <th>Nom et Prenom</th>
                        <th>Produit</th>
                        <th>Date Emprunt</th>
                        <th>Date Retour</th>
                        <th>Initiale Emprunt</th>
                        <th>Initiale Retour</th>
                     </tr>
                  </thead>
                  <tbody>                        
                           {EmpruntList.map((val)=>{
                              return <tr key={val.id_Emprunt}>
                                       <td>{val.id_Abonnes}</td>
                                       <td>{val.id_Produit}</td>
                                       <td>{val.date_Emprunt}</td>
                                       <td>{val.date_Retour}</td>
                                       <td>{val.initial_Emprunt}</td>
                                       <td>{val.initial_Retour}</td>
                                       <td><button onClick={()=>{deleteEmprunt(val.id_Emprunt)}}>Delete</button></td>
                                 </tr> 
                           })}
                  </tbody>
               </table>
               </div>
               <div>
                  <h2>Ajout Emprunt</h2>
                  {/* <label>Nom abonnes </label>
                  <select id="nomAbonnes" onChange={(e)=>{setIdAbonnes(e.target.value)}}>
                  {abonnesList.map((val)=>{ return <option key={val.idAbonnes} value={val.idAbonnes}> {val.nom} {val.prenom}</option>})}
                  </select> */}
                  
                  <label>Nom des Abonnes </label>
                  <select id="nomAbonnes" onChange={(e)=>{setIdAbonnes(e.target.value)}}>
                  {abonnesList.map((val)=>{ return <option key={val.id_Abonnes} value={val.id_Abonnes}>{val.nom} {val.prenom} </option>})}
                  </select>

                  <label>Code Produit </label>
                  <select id="codeProduit" onChange={(e)=>{setId_Produit(e.target.value)}}>
                  {produitList.map((val)=>{ return <option key={val.id_Produit} value={val.id_Produit}>{val.code_Produit} </option>})}
                  </select>
                  <label>Date Emprunt</label>
                  <input type="date" id="dateEmprunt" onChange={(e)=>{setDate_Emprunt(e.target.value) ;console.log(e.target.value)}}></input>
                  <label>INTIALE EMPRUNT</label>
                  <input type="text" id="initEmprunt" onChange={(e)=>{setInitial_Emprunt(e.target.value) ; console.log(e.target.value)}}></input>
                  <div>
                        <button className="buttonab" onClick={creerEmprunt}> Submit</button>
                  </div>
               </div> 
            </div>
      </div>
   )
}

export default Emprunt