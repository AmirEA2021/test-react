import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import '../style/abonnes.css';
import {Link, NavLink} from 'react-router-dom';




function Abonnes() {

    const [nom, setNom] = useState('')
    const [prenom, setPrenom] = useState('')
    const [courriel, setCourriel] = useState('')

    const [abonnesList, setAbonnesList] = useState([])

    const [abonnes, setAbonnes] = useState([])

    /* Affichage formulaire update */
    
    const [visible, setVisible] = useState(true);
    
   let idAb = null;

    function changement(val){

        /* idAb = val; */

        setVisible(!visible);

        /* console.log(idAb); */
        
        /* let test = false;

        if (visible===false && test===false) console.log("All is false"); */
               
    }

    const getOneAbonnes=(id)=>{
        Axios.get(`http://localhost:4000/api/getonea/${id}`).then((response)=>{
            console.log("GET ONE FUNCTION RESPONSE")
            setAbonnes(response.data); 
            console.log(response.data);

            /* setAbonnes(JSON.parse(response.data));                          
            console.log(abonnes); */

            /* console.log(abonnes); */
            setVisible(!visible);   
        /* setAbonnes(response.data);
            console.log(setAbonnes); */
            /* console.log(response.data); */

           /*  console.log(" Abonnes :");
            console.log(abonnes);
            console.log(" Nom de l'abonnes :");
            console.log(abonnes.nom);
            console.log(" ID abonnes :"); */
           

        })
    }
/* ******************************** TEST ************************************* */

/*     useEffect((id)=>{

        Axios.get(`http://localhost:4000/api/getonea/${id}`).then((response)=>{
        console.log(response.data);
        setAbonnes(response.data)
        })
           },[]) */
/* ******************************** TEST ************************************* */


    useEffect(()=>{

        Axios.get('http://localhost:4000/api/geta').then((response)=>{
        /* console.log(response.data); */
        setAbonnesList(response.data)
        })
           },[])




    const deleteAbonnes =(id)=>{
        Axios.delete(`http://localhost:4000/api/delete/${id}`);
    }



    const updateAbonnes=(id)=>{
        Axios.put(`http://localhost:4000/api/update/${id}`, {nom : nom, prenom : prenom, courriel : courriel}).then(()=>{
            console.log("Update avec succes");
        })
    }




    return (

        <div className="abonnes borderbib">
            <h2>Liste Abonnes</h2>
            <Link to="/ajoutAbonnes">Ajouter un nouveau abonné</Link>
            <div>
                <div>
                    
                    <table>
                        <thead>
                            <tr>
                                <th>ID Abonnes</th>
                                <th>Nom</th>
                                <th>Prenom</th>
                                <th>Courriel</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                            {abonnesList.map((val)=>{
                            return  <tr key={val.id_Abonnes}>
                                        <td>{val.id_Abonnes}</td>
                                        <td>{val.nom}</td>
                                        <td>{val.prenom}</td>
                                        <td>{val.courriel}</td>
                                        <td><button onClick={()=>{deleteAbonnes(val.id_Abonnes)}}>Delete</button></td>
                                        <td><Link to={`/EditAbonnes/${val.id_Abonnes}`} ><button>Mise à jour</button></Link></td>
                                    </tr>
                                                    })}
                        </tbody>
                    </table>
                </div>
            </div>
            {
                          !visible &&  (<form id="formUpdate" >
                                <h2>Modification Abonnes</h2>
                                <div>
                                    <label>Nom  </label>
                                    <input type="text" name="nom"  placeholder={abonnes.nom} onChange={(e)=>{
                                        setNom(e.target.value)
                                    }}></input>
                                </div>
                                <div>
                                    <label>Prenom </label>
                                    <input type="text" name="prenom" placeholder={abonnes.prenom} onChange={(e)=>{
                                        setPrenom(e.target.value)
                                    }}></input>
                                </div>
                                <div>
                                    <label>Courriel </label>
                                    <input type="text" name="courriel" onChange={(e)=>{
                                        setCourriel(e.target.value)
                                    }}></input>
                                </div>
                                <div>
                                    <button className="buttonab">Update</button>
                                </div>
                            </form>) }
        </div>

    )

 }


export default Abonnes