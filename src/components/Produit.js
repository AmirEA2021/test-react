import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import '../style/abonnes.css';
import Banner from './Banner';

function Produit() {

    const [id_Produit, setid_Produit] = useState('')
    const [nom, setNom] = useState('')
    const [description, setDescription] = useState('')
    const [code_Produit, setCode_Produit] = useState('')

/* ********************** UPDATE ******************************** */
    const [id_ProduitUPdate, setid_ProduitUPdate] = useState('')
    const [updateNom, setupdateNom] = useState('')
    const [updateDescription, setupdateDescription] = useState('')
    const [updateCode_Produit, setupdateCode_Produit] = useState('')

    const [produitList, setProduitList] = useState([])

    const [NewproduitList, setNewProduitList] = useState([])
     

    useEffect(()=>{

        Axios.get('http://localhost:4000/api/getp').then((response)=>{
        console.log(response.data);
        setProduitList(response.data)
        })
           },[])


      const submitProduit=()=>{
        Axios.post('http://localhost:4000/api/insertp', {nom : nom, description : description, code_Produit : code_Produit}).then(()=>{
            setProduitList([...produitList,{nom : nom, description : description, code_Produit : code_Produit}])
        });
    };


    const deleteProduit =(id)=>{
        Axios.delete(`http://localhost:4000/api/deletep/${id}`);
    }


 /*    const updateProduit=(id)=>{
        Axios.put(`http://localhost:4000/api/updatep/${id}`, {nom : updateNom, description : updateDescription, code_Produit : updateCode_Produit}).then(()=>{
            
        console.log("Update avec succes");
        })
        setNewProduitList("");
    } */




    return (
    <div>   
            <div><Banner/></div>
            <div className="abonnes borderbib">
                
                
                
                
                <h2>PRODUITS</h2>
                <div>
                    <div>
                        <label>Nom du Produit </label>
                        <input type="text" name="nom" onChange={(e)=>{
                            setNom(e.target.value)
                        }}></input>
                    </div>
                    <div>
                        <label>Description du produit </label>
                        <input type="text" name="dscription" onChange={(e)=>{
                            setDescription(e.target.value)
                        }}></input>
                    </div>
                    <div>
                        <label>Code du produit </label>
                        <input type="text" name="code_Produit" onChange={(e)=>{
                            setCode_Produit(e.target.value)
                        }}></input>
                    </div>
                    <div>
                        <button onClick={submitProduit} className="buttonab"> Submit</button>
                    </div>
                </div>
                <div>
                    <div>
                        <table>
                            <thead>
                                <tr>
                                    <th>Nom du Produit</th>
                                    <th>Description du produit</th>
                                    <th>Code du produit</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                {produitList.map((val)=>{
                                return <tr key={val.id_Produit}>
                                            <td>{val.nom}</td>
                                            <td>{val.description}</td>
                                            <td>{val.code_Produit}</td>
                                            <td><button onClick={()=>{deleteProduit(val.id_Produit)}}>Delete</button></td>
                                        </tr>
                                                        })}
                            </tbody>
                        </table>
                    </div>
                </div>

                <div>
                    <h2>Modification produit</h2>
                    <select id="nomProduit" onChange={(e)=>{setid_ProduitUPdate(e.target.value)}}>
                        {/* <options key="undefined" value="" selected>Selectionner produit</options> */}
                        {produitList.map((val)=>{ return <option key={val.id_Produit} value={val.id_Produit}>{val.nom}</option>})}
                    </select>

                <div>
                    <div>
                        <label>Nom du Produit </label>
                        <input type="text" name="nom" onChange={(e)=>{
                            setupdateNom(e.target.value)
                        }}></input>
                    </div>
                    <div>
                        <label>Description du produit </label>
                        <input type="text" name="dscription" onChange={(e)=>{
                            setupdateDescription(e.target.value)
                        }}></input>
                    </div>
                    <div>
                        <label>Code du produit </label>
                        <input type="text" name="code_Produit" onChange={(e)=>{
                            setupdateCode_Produit(e.target.value)
                        }}></input>
                    </div>
                    <div>
                        <button /* onClick={updateProduit(id_ProduitUPdate)} */ className="buttonab">Mise a jour</button>
                    </div>
                </div>

                </div>
                
            </div>
        </div>
    )

 }


export default Produit