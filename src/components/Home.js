import '../App.css';
import Banner from './Banner';
import Emprunt from './Emprunt';
import Abonnes from './Abonnes';
import Produit from './Produit';
import Statistique from './Statistique';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {Link, NavLink} from 'react-router-dom';



/* ************************* Page d'acceuil ************************* */

function Home() {

    return (
      
      
      <div className="Home">
        <Banner/>
        
        <button><NavLink to="/emprunt">Emprunts</NavLink></button>
        <button><NavLink to="/produit">Produits</NavLink></button>
        <button><NavLink to="/statistique">Statistique Bibliothèque</NavLink></button>
        <Abonnes/>
        
        
        
        
  
        
      </div>
      
    );
  }
  
  export default Home;
  