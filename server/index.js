const express = require("express")
const cors = require('cors')

const app = express()
const mysql = require('mysql')
const db = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'bibliotheque'
})
app.use(cors());
app.use(express.urlencoded({extended: true})); 
app.use(express.json()); 


/* ************************* Mise a jour abonnes ************************* */

app.put("/api/editabonnes/:id",(req, res)=>{

    const id = req.params.id;
    const nom = req.body.nom;
    const prenom = req.body.prenom;
    const courriel = req.body.courriel;

    const sqlUpdate = "UPDATE abonnes SET nom = ?, prenom = ? , courriel = ? WHERE id_Abonnes = ?";
        db.query(sqlUpdate, [nom, prenom, courriel, id], (err,result)=>{
        console.log(result);
           
    });
})

/* ************************* Mise a jour produit ************************* */

app.put("/api/updatep/:id",(req, res)=>{

    const id = req.params.id_Produit;
    const nom = req.body.nom;
    const description = req.body.description;
    const code_Produit = req.body.code_Produit;

    const sqlUpdate = "UPDATE produit SET nom = ?, description = ? , code_Produit = ? WHERE id_Produit = ?";
        db.query(sqlUpdate, [nom, description, code_Produit, id], (err,result)=>{
        console.log(result);
           
    });
})

/* ************************* Mise a jour Emprunt ************************* */

app.put("/api/updateem/:id",(req, res)=>{

    const id = req.params.id_Emprunt;
    const date_Retour = req.body.date_Retour;
    const initial_Retour = req.body.initial_Retour;
    
    const sqlUpdate = "UPDATE emprunt SET date_Retour = ?, initial_Retour = ?  WHERE id_Emprunt = ?";
        db.query(sqlUpdate, [date_Retour, initial_Retour, id], (err,result)=>{
        console.log(result);
           
    });
})

/* ************************* Mise a jour Emprunt ************************* */

app.put("/api/updatep/",(req, res)=>{

    const id = req.body.id_Produit;
    const nom = req.body.nom;
    const description = req.body.description;
    const code_Produit = req.body.code_Produit;

    
    const sqlUpdate = "UPDATE produit SET nom = ?, description = ?, code_Produit = ?  WHERE id_Produit = ?";
        db.query(sqlUpdate, [nom, description, code_Produit, id], (err,result)=>{
            if (err){console.log(err);}
            else {res.send(result); console.log(result);}
        
           
    });
})


/* ************************* supprimer produit ************************* */

app.delete("/api/deletep/:id",(req, res)=>{
    const id = req.params.id;
    const sqlDelete = "DELETE FROM produit WHERE id_Produit = ?";
    db.query(sqlDelete, id, (err, result)=>{
        if (err) console.log(err);
       
    });
})


/* ************************* supprimer abonnes ************************* */
app.delete("/api/delete/:id",(req, res)=>{
    const id = req.params.id;
    const sqlDelete = "DELETE FROM abonnes WHERE id_Abonnes = ?";
    db.query(sqlDelete, id, (err, result)=>{
        if (err) console.log(err);
       
    });
})

/* ************************* supprimer Emprunt ************************* */
app.delete("/api/deleteem/:id",(req, res)=>{
    const id = req.params.id;
    const sqlDelete = "DELETE FROM emprunt WHERE id_Emprunt = ?";
    db.query(sqlDelete, id, (err, result)=>{
        if (err) console.log(err);
       
    });
})


/* ************************* Liste Produit ************************* */
app.get("/api/getp",(req, res)=>{
  
    const sqlSelect = "SELECT * FROM produit ";
    db.query(sqlSelect, (err,result)=>{
        res.send(result);
        console.log(result);
    });
})

/* ************************* Liste Abonnes id ************************* */

app.get("/api/getonea/:id",(req, res)=>{
    const id = req.params.id;
    const sqlSelectOneA = "SELECT * FROM abonnes WHERE  id_Abonnes = ? ";
    db.query(sqlSelectOneA, id, (err,result)=>{
        if (err) console.log(err);
        res.send(result);
        console.log(result);
    });
})

/* ************************* Liste Abonnes ************************* */

app.get("/api/geta",(req, res)=>{
  
    const sqlSelect = "SELECT * FROM abonnes ";
    db.query(sqlSelect, (err,result)=>{
        res.send(result);
        console.log(result);
    });
})

/* ************************* get One Abonnes ************************* */
/* 
app.get("/api/geta/:id",(req, res)=>{
  
    const sqlSelect = "SELECT * FROM abonnes WHERE id_Abonnes = ? ";
    db.query(sqlSelect, (err,result)=>{
        res.send(result);
        console.log( "Erreur"+ err);
        console.log("Resultat"+ result);
    });
}) */

/* ************************* Liste Emprunt ************************* */
app.get("/api/get",(req, res)=>{
  
    const sqlSelect = "SELECT * FROM emprunt ";
    db.query(sqlSelect, (err,result)=>{
        res.send(result);
        console.log(result);
    });

})


/* ************************* Ajout abonnes ************************* */
app.post("/api/insert", (req, res)=>{

    const nom = req.body.nom;
    const prenom = req.body.prenom;
    const courriel = req.body.courriel;

    const sqlInsert = "INSERT into abonnes (nom, prenom, courriel) VALUES (?,?,?)";
    db.query(sqlInsert, [nom, prenom, courriel], (err,result)=>{
        console.log(result);
    });
});

/* ************************* Ajout Produit ************************* */
app.post("/api/insertp", (req, res)=>{

    const nom = req.body.nom;
    const description = req.body.description;
    const code_Produit = req.body.code_Produit;

    const sqlInsert = "INSERT into produit (nom, description, code_Produit) VALUES (?,?,?)";
    db.query(sqlInsert, [nom, description, code_Produit], (err,result)=>{
        console.log(result);
    });
});

/* ************************* Ajout Emprunt ************************* */

app.post("/api/inserte", (req, res)=>{

    const idAbonnes = req.body.id_Abonnes;
    const id_Produit = req.body.id_Produit;
    const date_Emprunt = req.body.date_Emprunt;
    const initial_Emprunt = req.body.initial_Emprunt;

    const sqlInsert = "INSERT into emprunt (id_Abonnes, id_Produit, date_Emprunt, initial_Emprunt) VALUES (?,?,?,?)";
    db.query(sqlInsert, [idAbonnes, id_Produit, date_Emprunt,initial_Emprunt], (err,result)=>{
        console.log(err);
        console.log(result);
    });
});




app.listen(4000, ()=>{

    console.log("Running on port 4000")

});